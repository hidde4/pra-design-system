import {
  ArgType,
  Controls,
  Description,
  DocsContainer,
  DocsContainerProps,
  Markdown,
  Primary,
  Source,
  Stories,
  Subtitle,
  Title,
} from '@storybook/blocks';
import { Preview } from '@storybook/react';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/alternate/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { PropsWithChildren } from 'react';

const previewTabs = {
  'storybookjs/notes/panel': { title: 'Documentation' },
  'storybook/docs/panel': { title: 'API' },
  canvas: { title: 'Design Tokens' },
};

const customViewports = {
  MobileScreen: {
    name: 'Mobile Screen',
    styles: {
      width: '393px',
      height: '800px',
    },
  },
};

// Configure @etchteam/storybook-addon-status
const addonStatus = {
  status: {
    statuses: {
      PRODUCTION: {
        background: '#088008',
        color: '#ffffff',
        description:
          'Used in production in a variety of situations, well tested, stable APIs, mostly patches and minor releases.',
      },
      BETA: {
        background: '#3065ee',
        color: '#ffffff',
        description:
          'Used in production in a specific situation, evolving APIs based on feedback, breaking changes are still likely.',
      },
      ALPHA: {
        background: '#e0bc2e',
        color: '#000000',
        description:
          'Used in prototypes and in projects that are still in development, breaking changes occur frequently and are not communicated.',
      },
      'WORK IN PROGRESS': {
        background: '#cc0000',
        color: '#ffffff',
        description:
          'Do not use in production. Does not follow semantic versioning and any published packages are for internal use only.',
      },
    },
  },
};

const PraRendererCodeBlock: React.FC<{
  praRendererComponentName: string;
  argTypes: { [key: string]: ArgType };
}> = ({ praRendererComponentName, argTypes }) => {
  console.log(argTypes);
  const parametersList = Object.keys(argTypes).reduce((parametersList: string[], key: string) => {
    const parameterProps = argTypes[key];
    const parameterName = parameterProps.name;
    let value = parameterProps?.table?.defaultValue?.summary;

    if (parameterName === 'children') {
      value = '[""]';
    }
    parametersList.push(`"${parameterName}": ${value || null}`);

    return parametersList;
  }, []);
  return (
    <Source
      code={`{\n"controlType": "${praRendererComponentName}",\n"parameters": {\n\t${parametersList.join(',\n\t')}\n}}`}
    />
  );
};
const PraVersionBlock: React.FC<{ context: DocsContainerProps['context'] }> = ({ context }) => {
  const defaultStory = context?.componentStories()?.[0];
  const {
    parameters: {
      ['--pra-component-design-version-name']: designVersionName,
      ['--pra-component-design-version-id']: designVersionId,
    },
  } = defaultStory;
  const versionLabel =
    'Based on version ```' +
    (designVersionName || designVersionId) +
    '``` of the PRA designs<br>[📏 Check the design file (Figma)]' +
    (designVersionId
      ? '(https://www.figma.com/file/nmi4JRz80FdZfnGqhMJoc4/PRA---Persoonlijke-Regeling-Assistent?type=design&version-id=' +
        designVersionId +
        ')'
      : '');
  return <Markdown>{versionLabel}</Markdown>;
};

const preview: Preview = {
  decorators: [
    (Story) => (
      <div
        className="pra-theme"
        style={{
          height: '100%',
        }}
      >
        <Story />
      </div>
    ),
  ],
  parameters: {
    actions: { argTypesRegex: '^on[A-Z].*' },
    docs: {
      container: ({ context, children }: PropsWithChildren<DocsContainerProps>) => {
        const stories = context?.componentStories();
        const defaultStory = stories.length > 0 && stories[0];
        const { argTypes = {}, parameters = {} } = defaultStory || {};
        const {
          ['--pra-renderer-component-name']: praRendererComponentName,
          ['--pra-render-in-mobile-viewport']: praRenderInMobileViewport,
        } = parameters;

        const hasNoStories = stories.length < 1;
        const hasMoreStories = stories.length > 1;
        const hasArguments = Object.keys(argTypes).length > 0;

        return (
          <DocsContainer context={context}>
            <style>
              {praRenderInMobileViewport
                ? `
              .sbdocs.sbdocs-preview {
                  max-width: calc(${customViewports.MobileScreen.styles.width} + 2rem);
                  max-height: calc(${customViewports.MobileScreen.styles.height} + 2rem);
              }
             .sb-story {
                height: calc(${customViewports.MobileScreen.styles.height} + 2rem);
              }
              .sb-story > * {
                height: 100%;
              }
          `
                : ''}
            </style>
            <>
              {hasNoStories
                ? children
                : [
                    <Title key="title" />,
                    <Subtitle key="subtitle" />,
                    <Description key="description" />,
                    <Primary key="primary-story" />,
                    hasArguments && [
                      <Markdown key="arguments-title">##Arguments</Markdown>,
                      <Controls key="arguments" />,
                    ],
                    hasMoreStories && [
                      <Markdown key="stories-title">##Stories</Markdown>,
                      <Stories key="stories" title="" />,
                    ],
                    !!praRendererComponentName && [
                      <Markdown key="render-code-block-title">## PRA front-end renderer example code</Markdown>,
                      <PraRendererCodeBlock
                        key="render-code-block"
                        praRendererComponentName={praRendererComponentName}
                        argTypes={argTypes}
                      />,
                    ],
                    <Markdown key="design-version-title">## PRA design version</Markdown>,
                    <PraVersionBlock key="design-version" context={context} />,
                  ]}
            </>
          </DocsContainer>
        );
      },
      visualViewport: 'MobileScreen',
    },
    previewTabs,
    addonStatus,
    options: {
      panelPosition: 'right',
    },
    viewport: { viewports: customViewports, defaultViewport: 'MobileScreen' },
  },
};

export default preview;
