import { dirname, join } from 'path';
/* eslint-env node */
module.exports = {
  stories: [
    '../src/**/*stories.@(js|jsx|mdx|ts|tsx)',
    '../../components-react/src/components/**/*stories.@(js|jsx|mdx|ts|tsx)',
  ],
  addons: [
    getAbsolutePath('@etchteam/storybook-addon-status'),
    getAbsolutePath('@storybook/addon-a11y'),
    getAbsolutePath('@storybook/addon-docs'),
    getAbsolutePath('@storybook/addon-viewport'),
    getAbsolutePath('@storybook/blocks'),
    getAbsolutePath('@storybook/preset-scss'),
  ],
  framework: {
    name: getAbsolutePath('@storybook/react-vite'),
    options: {},
  },
  core: {
    disableTelemetry: true,
  },
  docs: {
    autodocs: true,
  },
  features: {
    buildStoriesJson: true,
    storyStoreV7: true,
  },
  staticDirs: ['../../../proprietary/assets/src'],
  typescript: {
    reactDocgen: 'react-docgen-typescript',
  },
};

function getAbsolutePath(value: string): string {
  return dirname(require.resolve(join(value, 'package.json')));
}
