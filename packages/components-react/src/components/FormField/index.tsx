import HelpIcon from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Help';
import { ForwardedRef, forwardRef, PropsWithChildren } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/FormField/FormField.scss';
import { FormLabel } from '../FormLabel';

interface FormFieldSpecificProps {
  type: 'textbox' | 'checkbox';
  label?: string;
  indicatorInfo?: string;
}

export interface FormFielddProps extends FormFieldSpecificProps {}

export const FormField = forwardRef(
  ({ children, label, type, indicatorInfo }: PropsWithChildren<FormFielddProps>, ref: ForwardedRef<HTMLDivElement>) => {
    return (
      <div ref={ref} className={`pra-form-field ${type ? `pra-form-field-${type}` : ''}`}>
        {label && <FormLabel className="pra-form-field-label">{label}</FormLabel>}
        {indicatorInfo && (
          <span title={indicatorInfo} className="pra-form-field-indicator">
            {HelpIcon({})}
          </span>
        )}
        <div className="pra-form-field-input">{children}</div>
      </div>
    );
  },
);

FormField.displayName = 'FormField';
