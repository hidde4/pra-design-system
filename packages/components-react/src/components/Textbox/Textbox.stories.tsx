import ChevronDown from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronDown';
import Search from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Search';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Textbox/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Textbox } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof Textbox> = {
  component: Textbox,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Textbox',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'textbox',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof Textbox>;

export const NoIcons: Story = {
  args: {},
};
export const TwoIcons: Story = {
  args: {
    placeholder: 'Welke regeling zoek je?',
    iconStart: Search,
    iconEnd: ChevronDown,
  },
};

export const EndIcon: Story = {
  args: {
    placeholder: 'Zoeken in berichten...',
    iconEnd: Search,
  },
};

export const StartIcon: Story = {
  args: {
    placeholder: 'Zoeken...',
    iconStart: Search,
  },
};
export const ReadOnly: Story = {
  args: {
    placeholder: 'Zoeken...',
    readOnly: true,
  },
};
export const ReadOnlyWithIcon: Story = {
  args: {
    placeholder: 'Zoeken...',
    readOnly: true,
    iconStart: Search,
  },
};

export const Disabled: Story = {
  args: {
    placeholder: 'Zoeken...',
    disabled: true,
  },
};
export const DisabledWithIcon: Story = {
  args: {
    placeholder: 'Zoeken...',
    disabled: true,
    iconStart: Search,
  },
};

export const Invalid: Story = {
  args: {
    placeholder: 'Zoeken...',
    invalid: true,
  },
};
export const InvalidWithIcon: Story = {
  args: {
    placeholder: 'Zoeken...',
    invalid: true,
    iconStart: Search,
  },
};

export const IconNotFound: Story = {
  args: {
    placeholder: 'Zoeken...',
    iconStart: () => (
      <>
        <svg></svg>
      </>
    ),
  },
};
