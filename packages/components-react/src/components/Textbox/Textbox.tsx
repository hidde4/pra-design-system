import { Textbox as UtrechtTextbox, TextboxProps as UtrechtTextboxProps } from '@utrecht/component-library-react';
import { ForwardedRef, forwardRef, InputHTMLAttributes, SVGProps } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/Textbox/Textbox.scss';

interface TextboxSpecificProps {
  iconStart?: (props: SVGProps<SVGSVGElement>) => JSX.Element;
  iconEnd?: (props: SVGProps<SVGSVGElement>) => JSX.Element;
  invalid: UtrechtTextboxProps['invalid'];
}
export interface TextboxProps extends InputHTMLAttributes<HTMLInputElement>, TextboxSpecificProps {}

export const Textbox = forwardRef(({ ...props }: TextboxProps, ref: ForwardedRef<HTMLLabelElement>) => {
  const { iconStart, iconEnd, placeholder, id, ...otherProps } = props as TextboxProps;
  return (
    <label className="pra-textbox" ref={ref} htmlFor={id}>
      {iconStart && <span className="pra-icon pra-icon-start">{iconStart({})}</span>}
      <UtrechtTextbox
        {...(otherProps as InputHTMLAttributes<HTMLInputElement>)}
        placeholder={placeholder}
        type="text"
        id={id}
      />
      {iconEnd && <span className="pra-icon pra-icon-end">{iconEnd({})}</span>}
    </label>
  );
});

Textbox.displayName = 'Textbox';
