import { ForwardedRef, forwardRef, PropsWithChildren } from 'react';

import '@persoonlijke-regelingen-assistent/components-css/GridLayout/GridLayout.scss';

interface GridLayoutSpecificProps {
  templateColumns: number;
}

export interface GridLayoutdProps extends GridLayoutSpecificProps {}

export const GridLayout = forwardRef(
  ({ children, templateColumns = 6 }: PropsWithChildren<GridLayoutdProps>, ref: ForwardedRef<HTMLDivElement>) => {
    return (
      <div
        ref={ref}
        className="pra-grid-layout"
        style={
          {
            '--pra-grid-layout-template-columns': `${templateColumns}`,
          } as React.CSSProperties
        }
      >
        {children}
      </div>
    );
  },
);

GridLayout.displayName = 'GridLayout';
