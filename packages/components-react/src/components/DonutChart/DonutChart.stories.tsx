import ReadMe from '@persoonlijke-regelingen-assistent/components-css/DonutChart/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { DonutChart } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof DonutChart> = {
  component: DonutChart,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/DonutChart',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'donutchart',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof DonutChart>;

export const Default: Story = {
  args: {
    width: 320,
    height: 320,
    data: [
      { key: 'Te betalen bedrag', value: 10, isSelected: true },
      { key: 'Eerdere VA *', value: 30 },
      { key: 'Ingehouden loonheffing', value: 30 },
      { key: 'AOW', value: 20 },
      { key: 'Pensioen uitkering', value: 10 },
    ],
    showLabels: false,
    donutValue: '1000',
  },
};
