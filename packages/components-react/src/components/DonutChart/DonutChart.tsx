import { Group } from '@visx/group';
import { scaleOrdinal } from '@visx/scale';
import { Pie } from '@visx/shape';
import { ProvidedProps } from '@visx/shape/lib/shapes/Pie';
import { ForwardedRef, forwardRef, PropsWithChildren } from 'react';
import { ColorSample } from '../ColorSample';
import '@persoonlijke-regelingen-assistent/components-css/DonutChart/DonutChart.scss';

interface ElementData {
  key: string;
  value: number;
  isSelected?: boolean;
}
type DataType = ElementData[];

export interface DonutChartProps {
  width: number;
  height: number;
  data: DataType;
  margin?: typeof defaultMargin;
  showLabels: boolean;
  donutValue: string;
}
export interface DonutChartArcProps {
  donutChart: ProvidedProps<ElementData>;
  arc: {
    data: ElementData;
    endAngle: number;
    index: number;
    padAngle: number;
    startAngle: number;
    value: number;
  };
  index: number;
  arcFill: string;
  showLabel: boolean;
  arcStroke: string;
}

export interface DonutChartArcLabelProps {
  centroidX: number;
  centroidY: number;
  value: number;
}

function Arc({ donutChart, arc, index, arcFill, arcStroke }: DonutChartArcProps) {
  const { key, value, isSelected } = arc.data;
  const [centroidX, centroidY] = donutChart.path.centroid(arc);
  const arcPath = donutChart.path(arc) || undefined;
  const strokeWidth = isSelected ? 5 : 0;
  const hasSpaceForLabel = arc.endAngle - arc.startAngle >= 0.1;
  const showLabel = false;
  return (
    <g key={`arc-${key}-${index}`}>
      <path d={arcPath} fill={arcFill} stroke={arcStroke} strokeWidth={strokeWidth} />
      {showLabel && hasSpaceForLabel && <ArcLabel centroidX={centroidX} centroidY={centroidY} value={value} />}
    </g>
  );
}

function ArcLabel({ centroidX, centroidY, value }: DonutChartArcLabelProps) {
  return (
    <text x={centroidX} y={centroidY} dy=".33em" fill="#ffffff" fontSize={15} textAnchor="middle" pointerEvents="none">
      {value}
    </text>
  );
}

const colors = [
  { fill: 'var(--pra-color-secondary-600)', stroke: '#1D1D66' },
  { fill: '#BCDCFF', stroke: '#445577' },
  { fill: '#FFC989', stroke: '#776633' },
  { fill: '#0D2745', stroke: '#000811' },
  { fill: '#A4A4FF', stroke: '#444477' },
];
const defaultMargin = { top: 20, right: 20, bottom: 20, left: 20 };

export const DonutChart = forwardRef(
  (
    {
      width = 0,
      height = 0,
      data = [],
      margin = defaultMargin,
      showLabels,
      donutValue,
    }: PropsWithChildren<DonutChartProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    const ordinal = scaleOrdinal({
      domain: data.map((d) => d.key),
      range: colors,
    });

    const innerWidth = width - margin.left - margin.right;
    const innerHeight = height - margin.top - margin.bottom;
    const radius = Math.min(innerWidth, innerHeight) / 2;
    const centerY = innerHeight / 2;
    const centerX = innerWidth / 2;
    const top = centerY + margin.top;
    const left = centerX + margin.left;
    const value = (d: ElementData) => d.value;
    const sortedData = data.sort((arc1, arc2) => {
      return arc1.isSelected === arc2.isSelected ? 0 : arc1.isSelected ? 1 : -1;
    });

    return (
      <div ref={ref} className="pra-donutchart">
        <svg width={width} height={height}>
          <Group top={top} left={left}>
            <Pie
              data={sortedData}
              pieValue={value}
              pieSortValues={() => 1}
              outerRadius={radius}
              innerRadius={radius - 60}
              startAngle={16}
            >
              {(donutChart) => {
                return donutChart.arcs.map((arc, index) => {
                  const arcColors = ordinal(arc.data.key);

                  return (
                    <Arc
                      donutChart={donutChart}
                      arc={arc}
                      index={index}
                      arcFill={arcColors.fill}
                      arcStroke={arcColors.stroke}
                      showLabel={showLabels}
                    />
                  );
                });
              }}
            </Pie>
            <text
              dy=".33em"
              style={{
                fontSize: '20pt',
                fontWeight: '800',
                fontFamily: 'RijksoverheidSansWebText',
                color: 'var(--pra-color-secondary-600)',
              }}
              fill="var(--pra-color-secondary-600)"
              textAnchor="middle"
              pointerEvents="none"
            >
              € {donutValue}
            </text>
          </Group>
        </svg>
        <ul className="pra-donutchart-legend">
          {sortedData
            .slice()
            .reverse()
            .map((arcData, index) => {
              return (
                <li>
                  <ColorSample color={colors[index].fill} />
                  <label>{arcData.key}</label>
                </li>
              );
            })}
        </ul>
      </div>
    );
  },
);

DonutChart.displayName = 'DonutChart';
