import { ButtonHTMLAttributes, ForwardedRef, forwardRef } from 'react';
import { Button } from '../Button';

import '@persoonlijke-regelingen-assistent/components-css/ButtonBadge/ButtonBadge.scss';

interface ButtonBadgeSpecificProps {
  pressed?: boolean;
}
export interface ButtonBadgeProps extends ButtonHTMLAttributes<HTMLButtonElement>, ButtonBadgeSpecificProps {}

export const ButtonBadge = forwardRef(({ ...props }: ButtonBadgeProps, ref: ForwardedRef<HTMLButtonElement>) => {
  const { children, pressed } = props as ButtonBadgeProps;
  return (
    <Button ref={ref} className="pra-button-badge" appearance="primary-action-button" pressed={pressed} {...props}>
      {children}
    </Button>
  );
});

ButtonBadge.displayName = 'ButtonBadge';
