import { ForwardedRef, forwardRef, PropsWithChildren } from 'react';

import '@persoonlijke-regelingen-assistent/components-css/UnorderedList/UnorderedList.scss';

interface UnorderedListSpecificProps {}

export interface UnorderedListProps extends UnorderedListSpecificProps {}

export const UnorderedList = forwardRef(
  ({ children }: PropsWithChildren<UnorderedListProps>, ref: ForwardedRef<HTMLUListElement>) => {
    return (
      <ul ref={ref} className="pra-unordered-list">
        {children}
      </ul>
    );
  },
);

UnorderedList.displayName = 'UnorderedList';
