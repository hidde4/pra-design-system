import chevron_right from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/UnorderedList/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { UnorderedListItem } from '../UnorderedListItem';
import { UnorderedList } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof UnorderedList> = {
  component: UnorderedList,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/UnorderedList',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'unorderedlist',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof UnorderedList>;

export const WithMarkerSVG: Story = {
  args: {
    children: [
      <UnorderedListItem markerContent={chevron_right({ fillColor: '#FF9213' })}>
        Goed voorbereid op je pensioen
      </UnorderedListItem>,
      <UnorderedListItem markerContent={chevron_right({ fillColor: '#FF9213' })}>
        Goed voorbereid op je pensioen
      </UnorderedListItem>,
      <UnorderedListItem markerContent={chevron_right({ fillColor: '#FF9213' })}>
        Goed voorbereid op je pensioen
      </UnorderedListItem>,
    ],
  },
};

export const WithMarkerContent: Story = {
  args: {
    children: [
      <UnorderedListItem markerContent="?">Goed voorbereid op je pensioen</UnorderedListItem>,
      <UnorderedListItem markerContent="?">Goed voorbereid op je pensioen</UnorderedListItem>,
      <UnorderedListItem markerContent="?">Goed voorbereid op je pensioen</UnorderedListItem>,
    ],
  },
};

export const WithMixedMarkers: Story = {
  args: {
    children: [
      <UnorderedListItem markerContent="?">Goed voorbereid op je pensioen</UnorderedListItem>,
      <UnorderedListItem markerContent={chevron_right({ fillColor: '#FF9213' })}>
        Goed voorbereid op je pensioen
      </UnorderedListItem>,
      <UnorderedListItem markerContent="?">Goed voorbereid op je pensioen</UnorderedListItem>,
      <UnorderedListItem markerContent="?">Goed voorbereid op je pensioen</UnorderedListItem>,
    ],
  },
};
