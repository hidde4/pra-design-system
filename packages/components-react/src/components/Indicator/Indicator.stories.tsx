import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Indicator/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Indicator } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof Indicator> = {
  component: Indicator,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Indicator',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'indicator',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof Indicator>;

export const WithoutOrganisationName: Story = {
  args: {
    indicationInMins: 5,
    bookmarkOnClick: () => {
      console.log('bookmark');
    },
  },
};

export const WithOrganisationName: Story = {
  args: {
    ...WithoutOrganisationName.args,
    organisation: 'Naam organisatie',
  },
};
