import Bookmark from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Bookmark';
import Clock from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Clock';
import React, { ForwardedRef, forwardRef, HTMLAttributes, MouseEventHandler } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/Indicator/Indicator.scss';

interface IndicatorSpecificProps {
  organisation?: string;
  indicationInMins: number;
  bookmarkOnClick?: MouseEventHandler;
}
export interface IndicatorProps extends HTMLAttributes<HTMLDivElement>, IndicatorSpecificProps {}

export const Indicator = forwardRef(({ ...props }: IndicatorProps, ref: ForwardedRef<HTMLDivElement>) => {
  const { organisation, indicationInMins, bookmarkOnClick } = props as IndicatorProps;
  const indicatorLabel = <span className="pra-indicator-label">{`${indicationInMins} minuten`}</span>;
  const bookmarkIcon = (
    <span className="pra-indicator-bookmark-icon" onClick={bookmarkOnClick}>
      {Bookmark({})}
    </span>
  );
  return (
    <div className="pra-indicator" ref={ref} {...props}>
      {organisation
        ? [bookmarkIcon, Clock({}), indicatorLabel, `${organisation}`]
        : [Clock({}), indicatorLabel, bookmarkIcon]}
    </div>
  );
});

Indicator.displayName = 'Indicator';
