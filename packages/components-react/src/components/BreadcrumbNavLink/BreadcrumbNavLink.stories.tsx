import layout_grid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/BreadcrumbNavLink/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { BreadcrumbNav } from '../BreadcrumbNav';
import { Icon } from '../Icon';
import { BreadcrumbNavLink } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
const meta: Meta<typeof BreadcrumbNavLink> = {
  component: BreadcrumbNavLink,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],
  title: 'PRA-DS/Components/BreadcrumbNavLink',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'breadcumbnavlink',
    ['--pra-component-design-version-name']: '0.0.0',
  },
  decorators: [
    (Story) => (
      <BreadcrumbNav>
        <Story />
      </BreadcrumbNav>
    ),
  ],
};

export default meta;
type Story = StoryObj<typeof BreadcrumbNavLink>;

export const IconLink: Story = {
  args: {
    current: false,
    headingLevel: 1,
    disabled: false,
    href: '#',
    label: 'menu',
    index: 0,
    children: [<Icon>{layout_grid({})}</Icon>],
  },
};
export const TextLink: Story = {
  args: {
    ...IconLink.args,
    children: ['item2'],
  },
};
