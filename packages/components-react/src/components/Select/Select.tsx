import { Select as UtrechtSelect, SelectProps as UtrechtSelectProps } from '@utrecht/component-library-react';
import { ForwardedRef, forwardRef, InputHTMLAttributes, PropsWithChildren, SVGProps } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/Select/Select.scss';

interface SelectSpecificProps {
  iconStart?: (props: SVGProps<SVGSVGElement>) => JSX.Element;
  iconEnd?: (props: SVGProps<SVGSVGElement>) => JSX.Element;
  invalid?: UtrechtSelectProps['invalid'];
  busy?: UtrechtSelectProps['busy'];
  noscript?: UtrechtSelectProps['noscript'];
}
export interface SelectProps extends PropsWithChildren<SelectSpecificProps>, InputHTMLAttributes<HTMLSelectElement> {}

export const Select = forwardRef(({ ...props }: SelectProps, ref: ForwardedRef<HTMLLabelElement>) => {
  const { iconStart, iconEnd, id, children, ...otherProps } = props as SelectProps;
  return (
    <label className="pra-select" ref={ref} htmlFor={id}>
      {iconStart && <span className="pra-icon pra-icon-start">{iconStart({})}</span>}
      <UtrechtSelect {...(otherProps as InputHTMLAttributes<HTMLSelectElement>)} id={id}>
        {children}
      </UtrechtSelect>
      {iconEnd && <span className="pra-icon pra-icon-end">{iconEnd({})}</span>}
    </label>
  );
});

Select.displayName = 'Select';
