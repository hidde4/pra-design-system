import Gezicht from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak/Gezicht';
import { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren, ReactNode } from 'react';
import { BackLink } from '../BackLink';
import { HeaderTitle } from '../HeaderTitle';
import { SpeachBulb } from '../SpeachBulb';

import '@persoonlijke-regelingen-assistent/components-css/Header/Header.scss';

interface HeaderSpecificProps {
  headerTitle?: string;
  speachBulbContent?: ReactNode;
  headerSubContent?: ReactNode;
  backButtonAction?: Function;
  showAssistant?: boolean;
  assistantColor?: string;
}
export interface HeaderProps extends HTMLAttributes<HTMLDivElement>, HeaderSpecificProps {}

export const Header = forwardRef(
  (
    {
      headerTitle = '',
      speachBulbContent,
      headerSubContent,
      backButtonAction,
      showAssistant = true,
    }: PropsWithChildren<HeaderProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <div ref={ref} className="pra-header">
        <HeaderTitle
          leftNode={backButtonAction ? <BackLink onClick={(e) => backButtonAction(e)}>terug</BackLink> : null}
          headerTitle={headerTitle}
          rightNode={showAssistant ? Gezicht({}) : null}
        />
        <div ref={ref} className="pra-header-sub">
          {speachBulbContent && <SpeachBulb>{speachBulbContent}</SpeachBulb>}
          {headerSubContent}
        </div>
      </div>
    );
  },
);

Header.displayName = 'Header';
