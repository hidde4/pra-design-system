import bell_ringing from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/BellRinging';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Header/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Heading } from '../Heading';
import { NotificationReminder } from '../NotificationReminder';
import { Header } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof Header> = {
  component: Header,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Header',
  parameters: {
    layout: 'fullscreen',
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'header',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof Header>;

export const Default: Story = {
  args: {
    headerTitle: 'Dashboard',
    speachBulbContent: [<b>Hulp nodig?</b>],
    HeaderSubContent: [<Heading level={1}>Goedemiddag</Heading>],
  },
};

export const LargeMessage: Story = {
  args: {
    headerTitle: 'Dashboard',
    speachBulbContent: [
      <NotificationReminder
        icon={bell_ringing}
        title="Heb je je zorgtoeslag al aangevraagd?"
        message="Lorem ipsum dolor sit amet consectetur. Leo a sit eget tincidunt convallis. Tortor pretium mauris posuere amet egestas morbi ultricies vestibulum faucibus."
      />,
    ],
    HeaderSubContent: [<Heading level={1}>Goedemiddag</Heading>],
  },
};
