import user_exclamation from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/UserExclamation';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/InformationAlert/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { InformationAlert } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof InformationAlert> = {
  component: InformationAlert,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/InformationAlert',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'informationalert',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof InformationAlert>;

export const Default: Story = {
  args: {
    icon: user_exclamation,
    children: [
      <p>
        Het getoonde bedrag is variabel en afhankelijk van diverse <a href="#">factoren en keuzes</a>.
      </p>,
    ],
  },
};
