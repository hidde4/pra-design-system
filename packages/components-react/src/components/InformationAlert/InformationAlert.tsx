import '@persoonlijke-regelingen-assistent/components-css/InformationAlert/InformationAlert.scss';
import React, { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren, SVGProps } from 'react';

interface InformationAlertSpecificProps {
  icon?: (props: SVGProps<SVGSVGElement>) => JSX.Element;
}
export interface InformationAlertProps extends HTMLAttributes<HTMLInputElement>, InformationAlertSpecificProps {}

export const InformationAlert = forwardRef(
  ({ children, icon }: PropsWithChildren<InformationAlertProps>, ref: ForwardedRef<HTMLDivElement>) => {
    return (
      <div ref={ref} className="pra-information-alert">
        <span className="pra-information-alert-icon">{icon && icon({})}</span>
        <span className="pra-information-alert-content">{children}</span>
      </div>
    );
  },
);

InformationAlert.displayName = 'InformationAlert';
