import { ForwardedRef, forwardRef, PropsWithChildren, ReactNode } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/UnorderedListItem/UnorderedListItem.scss';

interface UnorderedListItemSpecificProps {
  markerContent?: string | ReactNode;
}

export interface UnorderedListItemProps extends UnorderedListItemSpecificProps {}

export const UnorderedListItem = forwardRef(
  ({ markerContent = '-', children }: PropsWithChildren<UnorderedListItemProps>, ref: ForwardedRef<HTMLLIElement>) => {
    console.log(typeof markerContent);
    return (
      <li ref={ref} className="pra-unordered-list-item">
        {typeof markerContent === 'string' && <span className="pra-unordered-list-item-marker">{markerContent}</span>}
        {typeof markerContent === 'object' && <span className="pra-unordered-list-item-marker">{markerContent}</span>}
        {children}
      </li>
    );
  },
);

UnorderedListItem.displayName = 'UnorderedListItem';
