import ReadMe from '@persoonlijke-regelingen-assistent/components-css/NotificationItem/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { NotificationItem } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof NotificationItem> = {
  component: NotificationItem,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/NotificationItem',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'notificationitem',
    ['--pra-component-design-version-name']: 'autosave - 4573412333',
    ['--pra-component-design-version-id']: '4573412333',
  },
};

export default meta;
type Story = StoryObj<typeof NotificationItem>;

export const Default: Story = {
  args: {
    title: 'Keuze orgaandonatie',
    timeIndication: '3u',
    message:
      'Sinds augustus 2021 is iedereen automatisch orgaandonor. Wil je dit of jouw voorkeuren aanpassen? Dat kan. Ga naar www.donorregister.nl',
  },
};
export const NewItem: Story = {
  args: {
    newItem: true,
    ...Default.args,
  },
};
