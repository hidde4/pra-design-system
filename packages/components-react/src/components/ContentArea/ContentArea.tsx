import { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren } from 'react';

import '@persoonlijke-regelingen-assistent/components-css/ContentArea/ContentArea.scss';

interface ContentAreaSpecificProps {}
export interface ContentAreaProps extends HTMLAttributes<HTMLInputElement>, ContentAreaSpecificProps {}

export const ContentArea = forwardRef(
  ({ ...props }: PropsWithChildren<ContentAreaProps>, ref: ForwardedRef<HTMLDivElement>) => {
    const { children } = props as HTMLAttributes<HTMLInputElement>;
    return (
      <div className="pra-content-area" ref={ref}>
        {children}
      </div>
    );
  },
);

ContentArea.displayName = 'ContentArea';
