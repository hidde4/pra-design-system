import { ForwardedRef, forwardRef, HTMLAttributes, MouseEventHandler, PropsWithChildren, ReactNode } from 'react';

import '@persoonlijke-regelingen-assistent/components-css/NavigationBarItem/NavigationBarItem.scss';

interface NavigationBarItemSpecificProps {
  selected: boolean;
  indicator: boolean;
  icon?: ReactNode;
  text: string;
  onClickHandler: MouseEventHandler;
}
export interface NavigationBarItemProps extends HTMLAttributes<HTMLLIElement>, NavigationBarItemSpecificProps {}

export const NavigationBarItem = forwardRef(
  (
    { selected = false, indicator = false, icon, text, onClickHandler }: PropsWithChildren<NavigationBarItemProps>,
    ref: ForwardedRef<HTMLLIElement>,
  ) => {
    return (
      <li
        ref={ref}
        key={`nav-item-${text}`}
        className={`pra-navigation-bar-item ${selected ? 'pra-navigation-bar-item-selected' : ''}`}
        onClick={onClickHandler}
      >
        <span className="pra-navigation-bar-item-icon">{icon ? icon : null}</span>
        <span className="pra-navigation-bar-item-text">{selected && text}</span>
        {indicator && <span className="pra-navigation-bar-item-indication-dot" />}
      </li>
    );
  },
);

NavigationBarItem.displayName = 'NavigationBarItem';
