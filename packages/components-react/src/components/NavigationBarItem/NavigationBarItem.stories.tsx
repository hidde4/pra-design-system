import nl_avatar from '@persoonlijke-regelingen-assistent/assets/dist/icons/custom/NlAvatar';
import layout_grid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import settings from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Settings';
import Vrijwilligerswerk from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak/Vrijwilligerswerk';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/NavigationBarItem/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { NavigationBarItem } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof NavigationBarItem> = {
  component: NavigationBarItem,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/NavigationBarItem',
  parameters: {
    chromatic: { viewports: [100] },
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'navigationbaritem',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof NavigationBarItem>;

export const Profiel: Story = {
  args: {
    selected: false,
    indicator: false,
    text: 'Profiel',
    icon: nl_avatar,
    onClickHandler: () => {},
  },
};
export const ProfielSelected: Story = {
  args: {
    ...Profiel.args,
    selected: true,
  },
};
export const ProfielIndicator: Story = {
  args: {
    ...Profiel.args,
    indicator: true,
  },
};
export const ProfielSelectedAndIndicator: Story = {
  args: {
    ...Profiel.args,
    selected: true,
    indicator: true,
  },
};

export const Dashboard: Story = {
  args: {
    selected: false,
    indicator: false,
    text: 'Dashboard',
    icon: layout_grid,
    onClickHandler: () => {},
  },
};
export const DashboardSelected: Story = {
  args: {
    ...Dashboard.args,
    selected: true,
  },
};
export const DashboardIndicator: Story = {
  args: {
    ...Dashboard.args,
    indicator: true,
  },
};
export const DashboardSelectedAndIndicator: Story = {
  args: {
    ...Dashboard.args,
    selected: true,
    indicator: true,
  },
};

export const MijnPRA: Story = {
  args: {
    selected: false,
    indicator: false,
    text: 'Mijn PRA',
    icon: Vrijwilligerswerk,
    onClick: () => {},
  },
};

export const MijnPRASelected: Story = {
  args: {
    ...MijnPRA.args,
    selected: true,
  },
};
export const MijnPRAIndicator: Story = {
  args: {
    ...MijnPRA.args,
    indicator: true,
  },
};
export const MijnPRASelectedAndIndicator: Story = {
  args: {
    ...MijnPRA.args,
    selected: true,
    indicator: true,
  },
};
export const Instellingen: Story = {
  args: {
    selected: false,
    indicator: false,
    text: 'Instellingen',
    icon: settings,
    onClickHandler: () => {},
  },
};

export const InstellingenSelected: Story = {
  args: {
    ...Instellingen.args,
    selected: true,
  },
};
export const InstellingenIndicator: Story = {
  args: {
    ...Instellingen.args,
    indicator: true,
  },
};
export const InstellingenSelectedAndIndicator: Story = {
  args: {
    ...Instellingen.args,
    selected: true,
    indicator: true,
  },
};
