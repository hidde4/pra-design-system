import React, { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren, ReactElement } from 'react';
import { Heading } from '../Heading';
import '@persoonlijke-regelingen-assistent/components-css/HeaderTitle/HeaderTitle.scss';

interface HeaderBarTitleSpecificProps {
  headerTitle: string;
  leftNode?: ReactElement | null;
  rightNode?: ReactElement | null;
}
export interface HeaderBarTitleProps extends HTMLAttributes<HTMLDivElement>, HeaderBarTitleSpecificProps {}

export const HeaderTitle = forwardRef(
  ({ headerTitle, leftNode, rightNode }: PropsWithChildren<HeaderBarTitleProps>, ref: ForwardedRef<HTMLDivElement>) => {
    return (
      <div ref={ref} className="pra-header-title">
        {leftNode && <span className="pra-header-title-node-left">{leftNode}</span>}
        <Heading level={3}>{headerTitle}</Heading>
        {rightNode && <span className="pra-header-title-node-right">{rightNode}</span>}
      </div>
    );
  },
);

HeaderTitle.displayName = 'HeaderTitle';
