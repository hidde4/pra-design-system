import Politiek from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak/Politiek';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/HeaderTitle/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { BackLink } from '../BackLink';
import { HeaderTitle } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof HeaderTitle> = {
  component: HeaderTitle,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/HeaderTitle',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'headertitle',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof HeaderTitle>;

export const Default: Story = {
  args: {
    leftNode: <BackLink>terug</BackLink>,
    headerTitle: 'Dashboard',
  },
};

export const WithTwoNodes: Story = {
  args: {
    leftNode: <BackLink>terug</BackLink>,
    headerTitle: 'Dashboard',
    rightNode: <>{Politiek}</>,
  },
};
