import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Dropdown/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Dropdown } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof Dropdown> = {
  component: Dropdown,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],
  title: 'PRA-DS/Components/Dropdown',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'dropdown',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof Dropdown>;

export const Default: Story = {
  args: {
    initCollapsed: true,
    title: 'Inkomenscomponenten',
    variant: 'Default',
    children: [
      <p>
        Lorem ipsum dolor sit amet consectetur. Amet hendrerit montes risus risus facilisis non urna. Viverra quam non
        nisl egestas eget amet.
      </p>,
      <p>
        Lorem ipsum dolor sit amet consectetur. Amet hendrerit montes risus risus facilisis non urna. Viverra quam non
        nisl egestas eget amet.
      </p>,
    ],
  },
};

export const DefaultExpanded: Story = {
  args: {
    ...Default.args,
    initCollapsed: false,
  },
};

export const Stappenplan: Story = {
  args: {
    ...Default.args,
    variant: 'Stappenplan',
  },
};

export const StappenplanExpanded: Story = {
  args: {
    ...Stappenplan.args,
    initCollapsed: false,
  },
};
export const NoLine: Story = {
  args: {
    ...Default.args,
    variant: 'No Line',
  },
};

export const NoLineExpanded: Story = {
  args: {
    ...NoLine.args,
    initCollapsed: false,
  },
};
