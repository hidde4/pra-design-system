import ChevronDown from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronDown';
import ChevronUp from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronUp';
import { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren, useState } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/Dropdown/Dropdown.scss';

interface DropdownSpecificProps {
  title?: string;
  initCollapsed?: boolean;
  variant: 'Default' | 'No Line' | 'Stappenplan';
}

export interface DropdownProps extends DropdownSpecificProps {}

export const Dropdown = forwardRef(
  (
    { title = '', initCollapsed = true, variant = 'No Line', ...otherProps }: PropsWithChildren<DropdownProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    const { children } = otherProps as HTMLAttributes<HTMLInputElement>;
    const [collapsed, setCollapsed] = useState<boolean>(initCollapsed);
    return (
      <div className="pra-dropdown" ref={ref}>
        <div
          className="pra-dropdown-header"
          onClick={() => {
            setCollapsed(!collapsed);
          }}
        >
          {['Stappenplan', 'No Line'].includes(variant) && (
            <div className="pra-dropdown-header--stappenplan-circle-container">
              <div className="pra-dropdown-header--stappenplan-circle"></div>
            </div>
          )}
          <h3 className="pra-dropdown-title">{title}</h3>
          {!collapsed && ChevronUp({})}
          {!!collapsed && ChevronDown({})}
        </div>
        {!collapsed && (
          <div
            className={`pra-dropdown-content ${
              ['Stappenplan', 'No Line'].includes(variant) &&
              `pra-dropdown-content--${variant.split(' ').join('-').toLowerCase()}`
            } `}
          >
            {children}
          </div>
        )}
      </div>
    );
  },
);

Dropdown.displayName = 'Dropdown';
