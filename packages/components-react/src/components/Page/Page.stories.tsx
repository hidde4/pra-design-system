import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Page/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Heading } from '../Heading';
import { IntroductionBackground } from '../IntroductionBackground';
import { Page } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/alternate/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof Page> = {
  component: Page,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Page',
  parameters: {
    layout: 'fullscreen',
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'page',
    ['--pra-component-design-version-name']: '0.0.0',
    ['--pra-render-in-mobile-viewport']: true,
  },
};

export default meta;
type Story = StoryObj<typeof Page>;

export const Default: Story = {
  args: {
    alternateTheme: false,
    backgroundFC: undefined,
    headerTitle: 'dashboard',
    headerSpeachBulbContent: 'Hulp nodig?',
    headerSubContent: <Heading level={3}>Informatie</Heading>,
    headerBackButtonAction: undefined,
    navigationHide: false,
    navigationSelectedItem: 'dashboard',
  },
};
export const WithBackground: Story = {
  args: {
    ...Default.args,
    backgroundFC: (props) => <IntroductionBackground {...props} />,
  },
};
export const AlternateTheming: Story = {
  args: {
    ...Default.args,
    alternateTheme: true,
    backgroundFC: (props) => <IntroductionBackground {...props} />,
  },
};
export const WithBackButton: Story = {
  args: {
    ...Default.args,
    headerBackButtonAction: () => {},
  },
};

export const WithoutNavigation: Story = {
  args: {
    ...Default.args,
    navigationHide: true,
  },
};
