import nl_avatar from '@persoonlijke-regelingen-assistent/assets/dist/icons/custom/NlAvatar';
import layout_grid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import settings from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Settings';
import { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren, ReactNode } from 'react';
import { Header } from '../Header';
import { NavigationBar } from '../NavigationBar';
import { NavigationBarItem } from '../NavigationBarItem';
import '@persoonlijke-regelingen-assistent/components-css/Page/Page.scss';

interface PageSpecificProps {
  alternateTheme?: boolean;
  backgroundFC?: React.FC<{}>;
  headerTitle?: string;
  headerSpeachBulbContent?: string;
  headerSubContent?: ReactNode;
  headerBackButtonAction?: Function;
  navigationHide?: boolean;
  navigationSelectedItem?: string;
}
export interface PageProps extends HTMLAttributes<HTMLDivElement>, PageSpecificProps {}

export const Page = forwardRef(
  (
    {
      children,
      headerTitle,
      headerSubContent,
      headerBackButtonAction,
      headerSpeachBulbContent,
      navigationSelectedItem,
      navigationHide,
      backgroundFC,
      alternateTheme,
      ...props
    }: PropsWithChildren<PageProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <div className={`${alternateTheme ? 'pra-alternate-theme' : ''} pra-page`} ref={ref} {...props}>
        {backgroundFC && backgroundFC({ className: 'pra-page-background' })}
        <header>
          <Header
            headerTitle={headerTitle}
            speachBulbContent={headerSpeachBulbContent}
            headerSubContent={headerSubContent}
            backButtonAction={headerBackButtonAction}
          />
        </header>
        <main>{children}</main>
        <footer>
          {!navigationHide && (
            <NavigationBar>
              <NavigationBarItem
                selected={navigationSelectedItem === 'Dashboard'}
                indicator={false}
                text="Dashboard"
                icon={layout_grid({})}
                onClickHandler={() => {}}
              />
              <NavigationBarItem
                selected={navigationSelectedItem === 'Profiel'}
                indicator={true}
                text="Profiel"
                icon={nl_avatar({ fillColor: 'var(--pra-color-white)' })}
                onClickHandler={() => {}}
              />
              <NavigationBarItem
                selected={navigationSelectedItem === 'Instellingen'}
                indicator={false}
                text="Instellingen"
                icon={settings({})}
                onClickHandler={() => {}}
              />
            </NavigationBar>
          )}
        </footer>
      </div>
    );
  },
);

Page.displayName = 'Page';
