import { ForwardedRef, forwardRef, InputHTMLAttributes, PropsWithChildren } from 'react';
import { Textbox } from '../Textbox';

import '@persoonlijke-regelingen-assistent/components-css/Currency/Currency.scss';

interface CurrencySpecificProps {
  currencySymbol?: string;
}
export interface CurrencyProps extends InputHTMLAttributes<HTMLInputElement>, CurrencySpecificProps {}

export const Currency = forwardRef(
  ({ currencySymbol, ...otherProps }: PropsWithChildren<CurrencyProps>, ref: ForwardedRef<HTMLDivElement>) => {
    const { required } = otherProps as InputHTMLAttributes<HTMLInputElement>;
    return (
      <div className="pra-currency" data-currencySymbol={currencySymbol} ref={ref}>
        <Textbox {...(otherProps as InputHTMLAttributes<HTMLInputElement>)} aria-required={required} type="number" />
      </div>
    );
  },
);

Currency.displayName = 'Currency';
