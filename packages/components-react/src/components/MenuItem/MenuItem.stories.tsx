import notebook from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Notebook';
import birthday_party from '@persoonlijke-regelingen-assistent/assets/dist/images/birthday-party.png';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/MenuItem/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { MenuItem } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof MenuItem> = {
  component: MenuItem,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/MenuItem',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'menuitem',
    ['--pra-component-design-version-name']: 'autosave - 4482676048',
    ['--pra-component-design-version-id']: '4482676048',
  },
};

export default meta;
type Story = StoryObj<typeof MenuItem>;

export const VerticalSmallWithIcon: Story = {
  args: {
    icon: notebook,
    label: 'Zorg',
    mode: 'vertical',
    size: 'small',
  },
};
export const VerticalMediumWithIcon: Story = {
  args: {
    ...VerticalSmallWithIcon.args,
    size: 'medium',
  },
};
export const VerticalLargeWithIcon: Story = {
  args: {
    ...VerticalSmallWithIcon.args,
    size: 'large',
  },
};
export const VerticalExtraLargeWithIcon: Story = {
  args: {
    ...VerticalSmallWithIcon.args,
    size: 'extra-large',
  },
};

export const HorizontalSmallWithIcon: Story = {
  args: {
    icon: notebook,
    label: 'Zorg',
    mode: 'horizontal',
    size: 'small',
  },
};
export const HorizontalMediumWithIcon: Story = {
  args: {
    ...HorizontalSmallWithIcon.args,
    size: 'medium',
  },
};
export const HorizontalLargeWithIcon: Story = {
  args: {
    ...HorizontalSmallWithIcon.args,
    size: 'large',
  },
};
export const HorizontalExtraLargeWithIcon: Story = {
  args: {
    ...HorizontalSmallWithIcon.args,
    size: 'extra-large',
  },
};

export const HorizontalMediumMultiline: Story = {
  args: {
    icon: notebook,
    label: 'Belastingen, uitkeringen en toeslagen',
    mode: 'horizontal',
  },
};

export const VerticalSmallWithImage: Story = {
  args: {
    imageSrc: birthday_party,
    label: 'Geldzorgen: Wat kan ik regelen?',
    mode: 'vertical',
    size: 'small',
  },
};
export const VerticalMediumWithImage: Story = {
  args: {
    ...VerticalSmallWithImage.args,
    size: 'medium',
  },
};

export const VerticalLargeWithImage: Story = {
  args: {
    ...VerticalSmallWithImage.args,
    size: 'large',
  },
};
export const VerticalExtraLargeWithImage: Story = {
  args: {
    ...VerticalSmallWithImage.args,
    size: 'extra-large',
  },
};

export const HorizontalSmallWithImage: Story = {
  args: {
    ...VerticalSmallWithImage.args,
    mode: 'horizontal',
  },
};

export const HorizontalMediumWithImage: Story = {
  args: {
    ...HorizontalSmallWithImage.args,
    size: 'medium',
  },
};

export const HorizontalLargeWithImage: Story = {
  args: {
    ...HorizontalSmallWithImage.args,
    size: 'large',
  },
};
export const HorizontalExtraLargeWithImage: Story = {
  args: {
    ...HorizontalSmallWithImage.args,
    size: 'extra-large',
  },
};
