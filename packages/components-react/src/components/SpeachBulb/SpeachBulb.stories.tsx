import bell_ringing from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/BellRinging';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/SpeachBulb/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { NotificationReminder } from '../NotificationReminder';
import { SpeachBulb } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof SpeachBulb> = {
  component: SpeachBulb,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/SpeachBulb',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'speachbulb',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof SpeachBulb>;

export const Default: Story = {
  args: { children: [<strong>Hulp nodig?</strong>] },
};

export const Reminder: Story = {
  args: {
    children: [
      <NotificationReminder
        icon={bell_ringing}
        title="Heb je je zorgtoeslag al aangevraagd?"
        message="Lorem ipsum dolor sit amet consectetur. Leo a sit eget tincidunt convallis. Tortor pretium mauris posuere amet egestas morbi ultricies vestibulum faucibus."
      />,
    ],
  },
};
