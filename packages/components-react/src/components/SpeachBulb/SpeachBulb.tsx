import { ForwardedRef, forwardRef, PropsWithChildren } from 'react';

import '@persoonlijke-regelingen-assistent/components-css/SpeachBulb/SpeachBulb.scss';

interface SpeachBulbSpecificProps {}

export interface SpeachBulbProps extends SpeachBulbSpecificProps {}

export const SpeachBulb = forwardRef(
  ({ children }: PropsWithChildren<SpeachBulbProps>, ref: ForwardedRef<HTMLDivElement>) => {
    return (
      <div ref={ref} className="pra-speach-bulb">
        {children}
      </div>
    );
  },
);

SpeachBulb.displayName = 'SpeachBulb';
